var todoApp = angular.module('todoApp', ['ngRoute']);

todoApp.config(['$routeProvider', function($routeProvider) {

  $routeProvider
  .when('/home', {
    templateUrl: 'views/dash.html',
    controller: 'TodoCtrl'
  })
  .otherwise({
    redirectTo: '/home'
  });
}]);

todoApp.factory('TodoService', function($http) {

  return {
    get : function() {
      return $http.get('/api/todos');
    },
    create : function(todoData) {
      return $http.post('/api/todos', todoData);
    },
    delete : function(id) {
      return $http.delete('/api/todos/' + id);
    }
  };

});

todoApp.controller('TodoCtrl', ['$scope', '$http', '$rootScope', 'TodoService', function($scope, $http, $rootScope, TodoService) {
  console.log("in the controller");

  $scope.formData = {
    text: ''
  };

  $scope.todos = [];
  TodoService.get().success(function(data){
    $scope.todos = data;
    console.log($scope.todos);
  });

  $scope.createTodo = function() {
    console.log("Add" + $scope.formData.text);

    if ($scope.formData.text != undefined) {

      TodoService.create($scope.formData).success(function(data) {
        $scope.loading = false;
        $scope.formData = {};
        $scope.todos = data;
      });

    }
  };

  $scope.deleteTodo = function(id) {

    TodoService.delete(id).success(function(data) {
      $scope.todos = data;
    });

  };
}]);

